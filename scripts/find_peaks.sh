#!/bin/bash -l

usage()
{
cat<<EOF
  Usage: $0 [options] 
    <-i experiment (i.e. treatment) bam [file]>
    <-n basename for naming output file(s) [string]>
    <-e effective genome size (bp) [int]>
    <-c control bam [file] (optional)>
    <-f minimum peak fold-change cutoff [int] (optional)>
    <-o outdir [dir] (optional)>
EOF
exit 1
}

while getopts 'i:n:e:c:f:o:' OPTION
do
  case $OPTION in
  i)    expt_bam="$OPTARG"
        ;;
  n)    basename="$OPTARG"
        ;;
  e)    effgsize="$OPTARG"
        ;;
  c)    ctl_bam="$OPTARG"
        ;;
  f)    min_foldch="$OPTARG"
        ;;
  o)    outdir="$OPTARG"
        ;;
  ?)    echo usage
        exit 1
        ;;
  esac
done

if [[ ! $expt_bam ]] || \
   [[ ! $basename ]] || \
   [[ ! $effgsize ]]; then
    echo "missing some arguments"
    usage
fi
if [[ ! -e $expt_bam ]]; then
    echo "INPUT BAM FILE: $expt_bam not found"
    exit 1
fi
if [[ $ctl_bam && ! -e $ctl_bam ]]; then
    echo "CONTROL BAM FILE: $ctl_bam not found"
    exit 1
fi

min_foldch=${min_foldch:="0"}
outdir=${outdir:="."}

echo "Peak-calling start:"
date +%T

# use macs2 to call the peaks, using a control .bam file if available
if [[ ! $ctl_bam ]]; then
    macs2 callpeak --treatment ${expt_bam} --format BAMPE --name "${basename}" --gsize ${effgsize} --outdir "${outdir}" \
    --call-summits --qvalue 0.05 --keep-dup all --nolambda \
    2> "${basename}_macs2_stats.txt"
else
    macs2 callpeak --treatment ${expt_bam} --control "${ctl_bam}" --format BAMPE --name "${basename}" --gsize ${effgsize} --outdir "${outdir}"\
    --call-summits --qvalue 0.05 --keep-dup all \
    2> "${basename}_macs2_stats.txt"
fi

# filter for peaks that are at least 3-fold above background
awk -v FCH=${min_foldch} '$7 >= FCH { print $0 }' ${outdir}/${basename}_peaks.narrowPeak > ${outdir}/${basename}_peaks_filt.narrowPeak

# count and save number of filtered peaks. use stdin to avoid printing filename
wc -l < ${outdir}/${basename}_peaks.narrowPeak > ${outdir}/${basename}_numpeaks.txt
wc -l < ${outdir}/${basename}_peaks_filt.narrowPeak > ${outdir}/${basename}_numpeaks_filt.txt

echo "Peak-calling finished:"
date +%T

