#!/usr/bin/env python

from Bio import SeqIO
import pandas as pd
import argparse
from os import path

### Parse command line arguments ###

parser = argparse.ArgumentParser(description="Make a fasta file for motif search within peak sequences.")
parser.add_argument('-narrowPeak', type=str, help="[file]; peaks in .narrowPeak format", required=True)
parser.add_argument('-out', type=str, help="[file]; output fasta name", required=True)
parser.add_argument('-ref', type=str, help="[file]; entire genome in fasta format", required=True)
parser.add_argument('-extend', type= str, help="[\"all\"] or [int]; if [int] write sequence at summit bp +/- [int]bp", required=True)
parser.add_argument('-maxpeaks', type=int, help="[int]; use at most [int] peaks (prioritizes highest fold-change)")
parser.add_argument('-weights', action='store_true', help="write meme-formatted weights, proportional to peak fold-changes")
parser.add_argument('-fimocoords', action='store_true', help="write genomic coords in each header in fimo format")
args = parser.parse_args()

infile = args.narrowPeak
outfile = args.out
ref = args.ref
extend_bp = args.extend
max_peaks = args.maxpeaks
weights = args.weights
fimocoords = args.fimocoords

### Function definitions ###

def get_summit_seq(summit, extend, record):
    extend_start = summit - extend
    extend_stop = summit + 1 + extend
    return record[extend_start:extend_stop].seq

def get_peak_seq(start, stop, record):
    return str(record[start:stop].seq)

### Read and process input data ###

# Parse all chromosomes in fasta file, put into dict
genome_seq = SeqIO.parse(ref, 'fasta')
chr_records = {}
for record in genome_seq:
    chr_records[record.name.split()[0]] = record

# Sort peaks by fold-change (descending)
peaks = pd.read_csv(infile, sep='\t', header=None)
peaks.columns=['chromosome', 'start', 'stop', 'peak_name', 'display_score', 'unused', 'fold-change', '-log10qvalue', '-log10pvalue', 'relative_summit']
peaks.sort_values(by=['fold-change'], inplace=True, ascending=False)

# If extend_bp == "all" and more than one summit identified for a peak,
# then only keep the entry for the tallest summit

num_summits = peaks.shape[0]
print("Total summits: "+str(num_summits))

if extend_bp == "all":
    foldch_maxes = peaks.groupby(['start', 'stop'])['fold-change'].transform(max)
    peaks = peaks.loc[peaks['fold-change'] == foldch_maxes]
    num_summits = peaks.shape[0]
    print("Since 'extend all' was specified, only the tallest summit per peak will be considered...")
    print("Result after filtering: "+str(num_summits))

if max_peaks is not None:
    print("Limiting fasta output to at most "+str(max_peaks)+" summits (prioritizing tallest).")
    peaks = peaks.head(max_peaks)

ref_name = path.basename(ref).replace('.fasta','')


### Write a fasta record for each peak ###
with open(outfile, 'w') as f:
    
    # write weights all in one record at the top
    if weights:
        # set meme weights by fold-change, normalized 0-1
        meme_weights = peaks['fold-change'] / peaks['fold-change'].max()
        f.write(">WEIGHTS")
        for w in meme_weights.values:
            f.write(" "+str(w))
        f.write("\n")
    
    # write the peak fasta records
    for i in peaks.index:
        peak_chr = peaks.loc[i,'chromosome']
        peak_name = peaks.loc[i,'peak_name']
        start_bp = peaks.loc[i,'start']
        stop_bp = peaks.loc[i,'stop']
        fold_ch = round(peaks.loc[i,'fold-change'],2)
        q_score = round(peaks.loc[i,'-log10qvalue'],2)

        # if going for summit +/- extend, adjust start and stop
        if extend_bp != "all":
            abs_summit = start_bp + peaks.loc[i, 'relative_summit']
            start_bp = abs_summit - int(extend_bp)
            stop_bp = abs_summit + int(extend_bp)
        
        # get actual sequence from start to stop from the correct chromosome
        seq = get_peak_seq(start=start_bp, stop=stop_bp, record=chr_records[peak_chr])

        # make headers
        if fimocoords:
            # convert location to 1-based for fimo
            header = ">"+peak_chr+":"+str(start_bp+1)+"-"+str(stop_bp+1)
        else:
            # more useful header for other analysis
            header = ">"+ref_name+'_'+peak_name+"_foldch="+str(fold_ch)+"_qscore="+str(q_score)+"_loc="+peak_chr+":"+str(start_bp+1)+"-"+str(stop_bp+1)

        # write record
        f.write(header+"\n"+seq+"\n")
