#!/usr/bin/env python

import pandas as pd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--bedfile")
parser.add_argument("--outfile")
parser.add_argument("--extend")

args = parser.parse_args()

infile = args.bedfile
outfile = args.outfile
extend_bp = int(args.extend)

peaks = pd.read_csv(infile, sep='\t', header=None)

# standard bedfile columns
peaks.columns=['chromosome', 'start', 'stop', 'peak_name', 'p_score']

peaks['extend_start'] = peaks['start'] - extend_bp
peaks['extend_stop'] = peaks['stop'] + extend_bp

# build new bedfile using adjusted starts and stops
peaks_extended = peaks[['chromosome', 'extend_start', 'extend_stop', 'peak_name', 'p_score']]

peaks_extended.to_csv(outfile, sep="\t", header=False, index=False)
