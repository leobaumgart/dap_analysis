#!/bin/bash -l

usage()
{
cat<<EOF
  Usage: $0 [options] 
    <-i experiment (i.e. treatment) bam [file]>
    <-j narrowPeak file from macs2 [file]>
    <-n basename for naming output file(s) [string]>
    <-s average fragment size [float]>
    <-a alignment rate [float]>
    <-c amplified input library? [boolean] >
    <-o outdir [dir] (optional)>
EOF
exit 1
}

while getopts 'i:j:n:s:a:o:c:' OPTION
do
  case $OPTION in
  i)    expt_bam="$OPTARG"
        ;;
  j)    peaks_narrow="$OPTARG"
        ;;
  n)    basename="$OPTARG"
        ;;
  s)    avg_fragsize="$OPTARG"
        ;;
  a)    align_rate="$OPTARG"
        ;;
  o)    outdir="$OPTARG"
        ;;
  c)    amplib="$OPTARG"
        ;;
  ?)    echo usage
        exit 1
        ;;
  esac
done

if [[ ! $expt_bam ]] || \
   [[ ! $peaks_narrow ]] || \
   [[ ! $basename ]] || \
   [[ ! $avg_fragsize ]] || \
   [[ ! $amplib ]] || \
   [[ ! $align_rate ]]; then
    echo "missing some arguments"
    usage
fi
if [[ ! -e $expt_bam ]]; then
    echo "INPUT BAM FILE: $expt_bam not found"
    exit 1
fi
if [[ ! -e $peaks_narrow ]]; then
    echo "NARROWPEAK FILE: $peaks_narrow not found"
    exit 1
fi

outdir=${outdir:="."}

echo "DAP stats start:"
date +%T

num_subpeaks=$(cat "${peaks_narrow}" | wc -l)

if [[ ${num_subpeaks} > 0 ]]; then

    # collapse sub-peaks to avoid double-counting multiple summits
    bedtools merge -i ${peaks_narrow} -c 4 -o collapse \
    > "${outdir}/${basename}_peaks_collapsed.bed"

    # count number of peaks
    num_peaks=$(cat "${outdir}/${basename}_peaks_collapsed.bed" | wc -l)

    # plot peak distribtuons
    plot_stats.py -narrowPeak ${peaks_narrow} -out "${outdir}/${basename}_peak_distribution.png"

    # name sort bam file to match up mates
    samtools sort -n -o "${outdir}/${basename}_namesort.bam" ${expt_bam}

    # calculate number of reads where at least one mate in pair overlaps peak
    paired_in_peaks=$(bedtools pairtobed \
    -abam "${outdir}/${basename}_namesort.bam" \
    -b "${outdir}/${basename}_peaks_collapsed.bed" \
    | samtools view -c -)
else

    paired_in_peaks=0
    num_peaks=0

fi

# count total number of reads in bam
aligned_reads=$(samtools view -c ${expt_bam})

# divide reads in peaks by total aligned reads
frip_score=$(echo "scale=5; ${paired_in_peaks}/${aligned_reads}" | bc -l)

cat <<-EOF > ${outdir}/${basename}_dap_stats.tsv
    Name	AlignedReads	AlignmentRate(%)	AvgFragmentSize(bp)	Amplified?	Peaks	Sub-peaks	FRIP
    ${basename}	${aligned_reads}	${align_rate}	${avg_fragsize}	${amplib}	${num_peaks}	${num_subpeaks}	${frip_score}
EOF


echo "DAP stats finished:"
date +%T
