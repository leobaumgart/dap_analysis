#!/bin/bash -l 

usage()
{
cat<<EOF
  Usage: $0 [options]

    <-p narrowPeak file from macs2 [file]>
    <-t bp to trim from peaks [int]>
    <-n basename for naming output file(s) [string]>
    <-r genome annotation gff [file]>
    <-o outdir [dir] (optional)>
EOF
exit 1
}

while getopts 'p:t:n:r:o:' OPTION
do
  case $OPTION in
  p)    peaks_narrow="$OPTARG"
        ;;
  t)    trim="$OPTARG"
        ;;
  n)    basename="$OPTARG"
        ;;
  r)    genes_gff="$OPTARG"
        ;;
  o)    outdir="$OPTARG"
        ;;
  ?)    echo usage
        exit 1
        ;;
  esac
done

if [[ ! $peaks_narrow ]] || \
   [[ ! $trim ]] || \
   [[ ! $basename ]] || \
   [[ ! $genes_gff ]]; then
    echo "missing some arguments"
    usage
fi
if [[ ! -e $peaks_narrow ]] || \
   [[ ! -e $genes_gff ]]; then
    echo "One or more arguments didn't exist"
    exit 1
fi
outdir=${outdir:="."}

echo "Assigning genes start"
date +%T

# use bedtools sort so it doesn't complain when there are multiple sub-peaks
bedtools sort -i "${peaks_narrow}" \
> "${outdir}/${basename}_peaks_sort.bed"


# collapse sub-peaks to avoid assigning duplicates for peaks with multiple summits
bedtools merge -i "${outdir}/${basename}_peaks_sort.bed" -c 4,5,6,7,8,9,10 -o collapse,max,distinct,max,max,max,collapse \
> "${outdir}/${basename}_peaks_collapse.bed"

# trim the edges of the peaks, remove any peaks that are <1bp
# also convert relative summits to absolute summits
awk -v OFS='\t' -v trim="${trim}" '{print$1,$2+trim,$3-trim,$4,$5,$6,$7,$8,$9,$2+$10}' "${outdir}/${basename}_peaks_collapse.bed" \
| awk '$3 > $2 {print$0}' \
> "${outdir}/${basename}_peaks_collapse_trim.bed"

# filter for genic peaks, sort by significance
bedtools intersect -a "${outdir}/${basename}_peaks_collapse_trim.bed" -b ${genes_gff} -f 0.5 -wo \
| sort -k8rn -k4 \
> "${outdir}/${basename}_overlap_genes.bed"

# filter for intergenic peaks
bedtools subtract -a "${outdir}/${basename}_peaks_collapse_trim.bed" -b ${genes_gff} \
> "${outdir}/${basename}_peaks_collapse_trim_intergenic.bed"

### assign genes to intergenic peaks
# find the closest genes that are oriented away from the intergenic region for each peak, once for each strand
bedtools closest -a "${outdir}/${basename}_peaks_collapse_trim_intergenic.bed" -b "${genes_gff}" -t first -id -D a \
| awk '$17=="-" {print $0}' > "${outdir}/${basename}_upstream_genes_minus.bed"

bedtools closest -a "${outdir}/${basename}_peaks_collapse_trim_intergenic.bed" -b "${genes_gff}" -t first -iu -D a \
| awk '$17=="+" {print $0}' > "${outdir}/${basename}_downstream_genes_plus.bed"

# combine for both strands and sort by significance
cat "${outdir}/${basename}_downstream_genes_plus.bed" "${outdir}/${basename}_upstream_genes_minus.bed" \
| sort -k8rn -k4 -k14n \
> "${outdir}/${basename}_assigned_genes.bed"

### clean up intermediate files
rm "${outdir}/${basename}_peaks_sort.bed"
rm "${outdir}/${basename}_peaks_collapse.bed"
rm "${outdir}/${basename}_peaks_collapse_trim.bed"
rm "${outdir}/${basename}_peaks_collapse_trim_intergenic.bed"
rm "${outdir}/${basename}_upstream_genes_minus.bed"
rm "${outdir}/${basename}_downstream_genes_plus.bed"

echo "Assigning genes finished"
date +%T
