#!/usr/bin/env python

import matplotlib
from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator
import pandas as pd
import numpy as np
import argparse

matplotlib.use('agg')

parser = argparse.ArgumentParser(description="Plot some peak stats using a .narrowPeak file.")
parser.add_argument('-narrowPeak', type=str, help="[file]; peaks in .narrowPeak format", required=True)
parser.add_argument('-out', type=str, help="[file]; output plot name", required=True)
args = parser.parse_args()

infile = args.narrowPeak
outfile = args.out

plt.style.use('seaborn')
peaktable = pd.read_csv(infile, sep='\t', header=None)
peaktable.columns=['chr', 'start', 'stop', 'name', 'display', 'unused_1',
                  'fold_change', 'pscore', 'qscore', 'rel_summit']

fig, ax = plt.subplots(1,2, figsize=(14,6))

# first plot on left is peak summits height vs q-value
ax[0].set_xlabel("Peak significance\n${-log_{10}}(q-value)$", fontsize=24)
ax[0].set_ylabel("Summit height\n$fold-enrichment$", fontsize=24)
ax[0].tick_params(width=1, length=4)
ax[0].yaxis.set_major_locator(MaxNLocator(integer=True))

ax[0].scatter(peaktable['fold_change'].values, peaktable['qscore'].values, s=25, alpha=0.9)

# second plot on right is histogram of peak q-values
ax[1].set_xlabel("Peak significance\n${-log_{10}}(q-value)$", fontsize=24)
ax[1].set_ylabel("Count", fontsize=24)
ax[1].hist(peaktable['qscore'].values, bins=30)

plt.savefig(fname=outfile, dpi=400, format='png', bbox_inches='tight')
