#!/usr/bin/env python

import argparse
import json
import pandas as pd
import subprocess
import os

### Parse arguments ###

parser = argparse.ArgumentParser()
parser.add_argument("-outdir", type=str, 
                    help="dir for .json output",
                    default="./jaws_inputs")
parser.add_argument("-genomedir", type=str,
                    help="Directory with genome files",
                    default="/global/dna/projectdirs/RD/DAPseq/cromwell_genomes/")
parser.add_argument("-adapters", type=str,
                    help="Path to adapters fasta file for trimming",
                    default="/global/projectb/sandbox/gaag/bbtools/data/adapters.fa")
parser.add_argument("-inputjson", type=str, required=True,
                    help="Path to json used for Clarity sample submission")
parser.add_argument("-maxfrags", type=int,
                    help="Max number of fragments to use for alignment (int)",
                    default=1000000)
parser.add_argument('-skipmotifs', action='store_true',
                    help="Set to False to skip motif calling (bool)")

args = parser.parse_args()

outdir = args.outdir
genomedir = args.genomedir
adapters = args.adapters
inputjson = args.inputjson
maxfrags = args.maxfrags
skipmotifs = args.skipmotifs

### Function definitions ###

def get_jamo_info(library_names):
    jamo_info = []
    for chunk in [library_names[i:i+500] for i in range(0, len(library_names), 500)]:
        p = subprocess.run(['jamo', 'info', 'library']+chunk, encoding='utf-8', stdout=subprocess.PIPE)
        jamo_info_response = p.stdout
        for result in jamo_info_response.strip().split('\n'):
            [library_name, fastq_path, status, jamo_id] = result.split()
            jamo_info.append({'library_name': library_name,
                                'fastq_path': fastq_path,
                                'status': status,
                                'jamo_id': jamo_id})

    return pd.DataFrame(jamo_info)

def read_genome_size(gsizefile):
    with open(gsizefile) as f:
        return int(f.readline())

def get_bt2index_list(bt2index_dir):
    return [os.path.join(bt2index_dir, f) for f in os.listdir(bt2index_dir)]

def fetch_purged(purged_libs):
    print("{} libraries found with status PURGED.".format(len(purged_libs)))

    if len(purged_libs) > 500:
        print("JAMO can't handle more than 500 libraries at once. Fetching multiple times...")

    for chunk in [purged_libs[i:i+500] for i in range(0, len(purged_libs), 500)]:
        print("Fetching {} libraries with command:".format(len(chunk)))
        fetch_command = ['jamo', 'fetch', '-w', 'library'] + chunk
        print(*fetch_command, sep=' ')
        print("Waiting...")
        subprocess.run(fetch_command, encoding='utf-8', stdout=subprocess.PIPE)

def parse_clarity_json(json_dict):
    ### loop through all wells to find all library names, populate relevant info into dataframe
    libraries_list = []

    for protein_plate in json_dict['protein-plates']:

        plate_barcode = protein_plate['plate-barcode']

        for well in protein_plate['wells']:

            tf_id = well['protein-id']
            tf_nickname = well['nickname']

            if tf_id == 'Control_TF_DNA':
                is_control = True
                tf_name = 'NegCtl'
            else:
                is_control = False
                tf_name = tf_nickname
                

            for library_organism in well['organism-indexes']:

                library_name = library_organism['demultiplexed-library-name']
                organism_sample_id = library_organism['sample-id']

                library_dict = {
                    'plate_barcode': plate_barcode,
                    'library_name': library_name,
                    'is_control': is_control,
                    'tf_name': tf_name,
                    'organism_sample_id': organism_sample_id
                }

                libraries_list.append(library_dict)

    ### combine list of library dicts into df
    return pd.DataFrame(libraries_list)

#####
### read input clarity json, get fastq paths, and make inputs.json for workflow
#####

### make outdir
os.mkdir(outdir)

### read input clarity json file
with open(inputjson) as f:
    clarity_metadata = json.load(f)

### make dict mapping sample IDs to organism identity and reference ids
sample_id_to_organism_info = {}

for organism_info in clarity_metadata['organisms']:
    sample_id = organism_info['sample-id']
    sample_id_to_organism_info[sample_id] = organism_info

### get number pcr cycles that were run for these samples
pcr_cycles = clarity_metadata['number-pcr-cycles-dap-amplification']

### parse samples into clarity json, summarize in pandas dataframe
print("Parsing clarity metadata.json file.")
library_info_df = parse_clarity_json(clarity_metadata)

### lookup fastq paths in jamo, fetch purged files
print("Getting fastq paths from JAMO...")
jamo_info = get_jamo_info(library_info_df['library_name'].tolist())
#########################
#########################
## add proper handling for libraries that return more than one fastq
#########################
#########################
purged_libs = jamo_info[jamo_info['status'] == "PURGED"]
if purged_libs.shape[0] > 0:
    fetch_purged(purged_libs['library_name'].tolist())

### add fastq paths to summarized samples df
library_name_to_fastq_path = jamo_info.set_index('library_name')['fastq_path'].to_dict()
library_info_df['fastq_path'] = library_info_df['library_name'].map(library_name_to_fastq_path)

### check for missing fastq paths
missing_fastq_paths = library_info_df[library_info_df['fastq_path'].isnull()]
if len(missing_fastq_paths > 0):
    print("ERROR: One more more libraries were not found in JAMO:")
    print(*missing_fastq_paths['library_name'].values, sep='\n')
    exit(1)

print("Generating inputs.json file for each plate + organism combination...")
### group by each plate + organism combo
### keep plates separate since we only want to use the control samples from each corresponding plate
for (plate_barcode, organism_sample_id), group in library_info_df.groupby(['plate_barcode', 'organism_sample_id']):

    organism_info = sample_id_to_organism_info[organism_sample_id]
    organism_name = organism_info['genus'] + "_" + organism_info['species'] + "_" + organism_info['strain']
    print(f"Plate: {plate_barcode}\tOrganism: {organism_name}\tOrganism-Sample-ID: {organism_sample_id}")

    ref_repo_fasta_id = organism_info['reference'] ## not used yet, for rqc implementation
    ref_repo_gff_id = organism_info['annotation'] ## not used yet, for rqc implementation
    print(f'\tRef-Repo-IDs:\tfasta: {ref_repo_fasta_id}\tgff: {ref_repo_gff_id}')

#######
####### THIS BLOCK WILL CHANGE TO USE RQC REFERENCE REPO
#######
    # read genome size from file
    try:
        effgsize_file = os.path.join(genomedir,organism_name,organism_name+'.gsize')
        effgsize = read_genome_size(effgsize_file)
    except FileNotFoundError as e:
        print(e)
        print(f"Something is wrong with the gsize file: {effgsize_file}")
        exit(1)

    ref_fasta_file = os.path.join(genomedir, organism_name, organism_name+'.fasta')
    bt2index_list = get_bt2index_list(os.path.join(genomedir, organism_name, organism_name+'_bt2index'))
    ref_gff_file = os.path.join(genomedir, organism_name, organism_name+'_filt.gff')
    bgmodel_file = os.path.join(genomedir, organism_name, organism_name+'.bgmodel')
#######
####### RQC REF REPO CHANGES END HERE
#######
        
    ctl_raw_fastqs = group[group['is_control'] == True]['fastq_path'].tolist()
    expt_raw_fastqs = group[group['is_control'] == False]['fastq_path'].tolist()
    library_names_map = group.set_index('fastq_path')['library_name'].str.replace(':','_').to_dict()
    sample_names_map = group.set_index('fastq_path')['tf_name'].str.replace(':','_').to_dict()

    # make dict for output as JSON
    inputsJSON = {
        'jgi_dap_leo.adapters': adapters,
        'jgi_dap_leo.genome_fasta': ref_fasta_file,
        'jgi_dap_leo.bt2index_list': bt2index_list,
        'jgi_dap_leo.effgsize': effgsize,
        'jgi_dap_leo.genes_gff': ref_gff_file,
        'jgi_dap_leo.bgmodel': bgmodel_file,
        'jgi_dap_leo.amplified': str(pcr_cycles),
        'jgi_dap_leo.maxfrags': maxfrags,
        'jgi_dap_leo.find_motifs': not skipmotifs,
        'jgi_dap_leo.ctl_raw_fastqs': ctl_raw_fastqs,
        'jgi_dap_leo.expt_raw_fastqs': expt_raw_fastqs,
        'jgi_dap_leo.library_names_map': library_names_map,
        'jgi_dap_leo.sample_names_map': sample_names_map
    }

    # write JSON to file
    with open(os.path.join(outdir, organism_name+'_inputs.json'), 'w') as outfile:
        json.dump(inputsJSON, outfile, indent=4)

