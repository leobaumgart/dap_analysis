#!/usr/bin/env python

import argparse
import json
import pandas as pd
import subprocess
import os

### Parse arguments ###

parser = argparse.ArgumentParser()
parser.add_argument("-outfile", type=str, 
                    help="Name for output tsv file",
                    default="library_metadata.tsv")
parser.add_argument("-infile", type=str, required=True,
                    help="Path to json used for Clarity sample submission")

args = parser.parse_args()

outfile = args.outfile
inputjson = args.infile

### Function definitions ###

def get_jamo_info(library_names):
    jamo_info = []
    for chunk in [library_names[i:i+500] for i in range(0, len(library_names), 500)]:
        p = subprocess.run(['jamo', 'info', 'library']+chunk, encoding='utf-8', stdout=subprocess.PIPE)
        jamo_info_response = p.stdout
        for result in jamo_info_response.strip().split('\n'):
            [library_name, fastq_path, status, jamo_id] = result.split()
            jamo_info.append({'library_name': library_name,
                                'fastq_path': fastq_path,
                                'status': status,
                                'jamo_id': jamo_id})

    return pd.DataFrame(jamo_info)

def fetch_purged(purged_libs):
    print("{} libraries found with status PURGED.".format(len(purged_libs)))

    if len(purged_libs) > 500:
        print("JAMO can't handle more than 500 libraries at once. Fetching multiple times...")

    for chunk in [purged_libs[i:i+500] for i in range(0, len(purged_libs), 500)]:
        print("Fetching {} libraries with command:".format(len(chunk)))
        fetch_command = ['jamo', 'fetch', '-w', 'library'] + chunk
        print(*fetch_command, sep=' ')
        print("Waiting...")
        subprocess.run(fetch_command, encoding='utf-8', stdout=subprocess.PIPE)

def parse_organisms(clarity_json_dict):
    
    ### returns dict mapping sample IDs to all other organism info
    ### including genus/species/strain and reference ids
    
    sample_id_to_organism_info = {}
    for organism_info in clarity_json_dict['organisms']:
        sample_id = organism_info['sample-id']
        sample_id_to_organism_info[sample_id] = organism_info
        
    return sample_id_to_organism_info

def parse_clarity_json(clarity_json_dict):
    ### returns pandas dataframe with relevant library metadata

    ### extract organism info for later mapping to each library
    sample_id_to_organism_info = parse_organisms(clarity_json_dict)  

    ### loop through all wells to find all library names,
    ### populate relevant info into dataframe
    ### also add organism info for each library
    libraries_list = []

    for protein_plate in clarity_json_dict['protein-plates']:

        plate_barcode = protein_plate['plate-barcode']

        for well in protein_plate['wells']:

            tf_id = well['protein-id']
            tf_nickname = well['nickname']

            if tf_id == 'Control_TF_DNA':
                is_control = True
                tf_name = 'negative-control'
            else:
                is_control = False
                tf_name = tf_nickname


            for library_organism in well['organism-indexes']:

                library_name = library_organism['demultiplexed-library-name']
                organism_sample_id = library_organism['sample-id']

                library_dict = {
                    'plate_barcode': plate_barcode,
                    'library_name': library_name,
                    'is_control': is_control,
                    'tf_name': tf_name,
                    'tf_id': tf_id,
                    'organism_sample_id': organism_sample_id
                }

                ### append dict of organism info, based on sample_id
                organism_info = sample_id_to_organism_info[organism_sample_id]
                library_dict = {**library_dict, **organism_info}

                libraries_list.append(library_dict)


    ### combine list of library dicts into df
    return pd.DataFrame(libraries_list)


#####
### 1. read and parse input clarity json to dataframe
### 2. get fastq paths from jamo, fetch purged libraries
### 3. save metadata to tsv file
#####

### read input clarity json file
with open(inputjson) as f:
    clarity_metadata = json.load(f)

### parse samples in clarity json, summarize in pandas dataframe
print("Parsing clarity metadata json file.")
library_metadata = parse_clarity_json(clarity_metadata)

### lookup fastq paths in jamo, fetch purged files
print("Getting fastq paths from JAMO...")
jamo_info = get_jamo_info(library_metadata['library_name'].tolist())
#########################
#########################
## add proper handling for libraries that return more than one fastq
#########################
#########################
purged_libs = jamo_info[jamo_info['status'] == "PURGED"]
if purged_libs.shape[0] > 0:
    fetch_purged(purged_libs['library_name'].tolist())

### add fastq paths to summarized samples df
library_name_to_fastq_path = jamo_info.set_index('library_name')['fastq_path'].to_dict()
library_metadata['fastq_path'] = library_metadata['library_name'].map(library_name_to_fastq_path)

### check for missing fastq paths
missing_fastq_paths = library_metadata[library_metadata['fastq_path'].isnull()]
if len(missing_fastq_paths > 0):
    print("ERROR: One more more libraries were not found in JAMO:")
    print(*missing_fastq_paths['library_name'].values, sep='\n')
    exit(1)

library_metadata.to_csv(outfile, index=False, sep='\t')

print(f"Saved output to {outfile}")
